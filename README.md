# Traval 东阳党建管理

# 线上地址  http://partybuilding.server.zhhost.top/#/
登录账号
    ```
        账号 :  admin
        密码: 1 
        验证码: 1 
    ```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# vue-cli@4.5 开发配置
```javascript
+ vue create vuepro // 实例化项目
+ cd vuepro  // 进入项目
+ vue add router  // 引入路由
+ vue add typescript  // 引入ts
+ vue add axios  //-----这里的问题注意看plugins/axios.ts
// + vue add vuex  // 引入vuex 暂时没用到
+ vue add element-plus  // 使用element-plus

```

## 开发规则
 + 弹框/draw规则：编辑内容过少（需要用户输入或者上传的内容少于三项）做成弹框的形式 / （多余三项）内容过多采用draw抽屉的形式
 + 通用css规范 element-vriable.less
 + 通用js方法 utils/utils.ts


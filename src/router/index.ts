import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import store from "../store";
// 默认路由 普通用户
const routes = [
    { path: '/', name: 'Login', component: Login },
    {
        path: '/Home', name: "Home", component: Home,
        children: [
            { path: '/systemmanage', name: "systemmanage", component: () => import('../views/systemmanage/list.vue') },
            { path: '/organizationmanage', name: "organizationmanage", component: () => import('../views/organizationmanage/list.vue') },
            { path: '/partyaffairsmanage', name: "partyaffairsmanage", component: () => import('../views/partyaffairsmanage/list.vue') },
            { path: '/documentmanage', name: "documentmanage", component: () => import('../views/documentmanage/list.vue') },
        ]
    },

];
// 初始化 默认路由
const router = createRouter({
    history: createWebHashHistory(),
    routes
});
export default router

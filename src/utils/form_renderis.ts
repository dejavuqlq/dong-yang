// 内容管理公共数据资源配置
export default {
    memberRenderForms: [// 党员 渲染
      {
        title: '姓名:',
        type: 'input',
        inputType: 'text',
        key: 'name',
        placeholder: '请输入内容',
        maxlength: 20,
      },
      {
        title: '性别:',
        type: 'radio',
        options: ['男','女'],
        key: 'sex',
      },
      {
        title: '身份证号:',
        type: 'input',
        inputType: 'text',
        key: 'idcard',
        placeholder: '请输入身份证号',
        maxlength: 18,
      }, {
        title: '手机号:',
        type: 'input',
        inputType: 'number',
        key: 'mobile',
        placeholder: '请输入手机号',
        minlength: 11,
        maxlength: 11,
      },
      {
        title: '入党日期:',
        type: 'date',
        key: 'joinday',
      },
      {
        title: '所属组织:',
        type: 'cascader',
        key: 'org_id',
        checkStrictly: true,//可单选
        showalllevels: false,//仅显示选中
        placeholder: '请选择',
        show: true,
      },
      {
        title: '等级:',
        type: 'select',
        options: [
          { value: '1', label: '普通党员' },
          { value: '2', label: '科室领导' },
          { value: '3', label: '分管领导' },
          { value: '4', label: '街道主要领导' },
          // { value: '5', label: '街道书记' },
          // { value: '6', label: '承办人' },
        ],
        key: 'leave',
        placeholder: '请选择',
      },
      {
        title: '个人特长:',
        type: 'input',
        inputType: 'text',
        key: 'specialty',
        placeholder: '请输入特长，并以"，"隔开',
        maxlength: 30,
      },
      {
        title: '党员状态:',
        type: 'switch',
        activevalue: 1,
        activetext: '合格',
        inactivevalue: 0,
        intext: '不合格',
        key: 'state_member',
      },
      {
        title: '账号状态:',
        type: 'switch',
        activevalue: 1,
        activetext: '锁定',
        inactivevalue: 0,
        intext: '未锁定',
        key: 'state_user',
      },
    ],
    memberForms: {// 党员 表单
      id:0,
      name:"",
      sex: "0",
      idcard:"",
      mobile:"",
      joinday:"",
      org_id:0,
      leave:"",
      specialty:"",
      state_member:"1",
      state_user:"1",
    },
    memberRules: {// 党员 验证
      name: [{ required: true, message: '请输入姓名', trigger: 'change' }],
      sex: [{ required: true, message: '请选择性别', trigger: 'change' }],
      // idcard: [{ required: true, message: '请输入身份证号', trigger: 'change' }],
      mobile: [{ required: true, message: '请输入手机号', trigger: 'change' }],
      joinday: [{ required: true, message: '请选择入党日期', trigger: 'change' }],
      // organization_id: [{ required: true, message: '请选择所属组织', trigger: 'change' }],
      // dengji: [{ required: true, message: '请选择等级', trigger: 'change' }]
    },
    recordRenderForms: [
      {
        title: '原组织:',
        type: 'cascader',
        key: 'source_id',
        checkStrictly: true,//可单选
        showalllevels: false,//仅显示选中
        placeholder: '请选择',
      },
      {
        title: '原组织(系统外):',
        type: 'input',
        inputType: 'text',
        key: 'source_org',
        placeholder: '请输入内容',
        maxlength: 20,
      },
      {
        title: '新组织:',
        type: 'cascader',
        key: 'target_id',
        checkStrictly: true,//可单选
        showalllevels: false,//仅显示选中
        placeholder: '请选择',
      },
      {
        title: '新组织(系统外):',
        type: 'input',
        inputType: 'text',
        key: 'target_org',
        placeholder: '请输入内容',
        maxlength: 20,
      },
      {
        title: '流转时间:',
        type: 'date',
        key: 'flow_time',
      },
    ],
    recordForms: {// 编辑/新增 表单提交
      member_id:"",
      source_id:"",
      source_org:"",
      target_id:"",
      target_org:"",
      flow_time:""
    },
    recordRules: {// 编辑/新增 表单验证规则
      flow_time: [{ required: true, message: '请选择流转时间', trigger: 'change' }],
    },
    organizeRenderForms: [
      {
        title: '组织名称:',
        type: 'input',
        inputType: 'text',
        key: 'name',
        placeholder: '请输入组织名称',
        maxlength: 20,
      },
      {
        title: '上级组织:',
        type: 'cascader',
        key: 'superior',
        checkStrictly: true,//可单选
        showalllevels: false,//仅显示选中
        show: true,
        options: []
      },
      // {
      //   title: '组织书记:',
      //   type: 'leader',
      //   options: [
      //     { value: 0, label: '党员a' },
      //     { value: 1, label: '党员b' },
      //     { value: 2, label: '党员c' },
      //   ],
      //   key: 'shuji',
      //   placeholder: '请选择',
      // },
      {
        title: '组织成员:',
        type: 'custom',
        show: true,
        key: 'users',
      },
    ],
    organizerForms: {//组织 表单
      id: '',
      name: '',
      superior: null,
      users: [],
    },
    organizeRules: {// 编辑/新增 表单验证规则
      name: [{ required: true, message: '请输入组织名称', trigger: 'change' }],
      // superior: [{ required: true, message: '请选择上级组织', trigger: 'change' }],
    },
    duesRenderForms: [
      {
        title: '缴纳额:',
        type: 'input',
        inputType: 'number',
        key: 'number',
        placeholder: '请输入内容',
        maxlength: 4,
      },
      {
        title: '缴纳时间:',
        type: 'date',
        key: 'dues_time',
      },
      // {
      //     title: '应缴时间:',
      //     type: 'date',
      //     key: 'c6',
      // },
    ],
    duesForms: {// 编辑/新增 表单提交
      member_id: 0,
      number: 0,
      dues_time: '',
      c7: ''
    },
    duesRules: {// 编辑/新增 表单验证规则
      number: [{ required: true, message: '请输入实际缴纳金额', trigger: 'change' }],
    },
    noticeRenderForms: [
      {
        title: '标题:',
        type: 'input',
        inputType: 'text',
        key: 'title',
        placeholder: '请输入内容',
        maxlength: 20,
      },
      {
        title: '副标题:',
        type: 'input',
        inputType: 'text',
        key: 'subtitle',
        placeholder: '请输入内容',
        maxlength: 20,
      },
      {
        title: '时间:',
        type: 'date',
        key: 'notice_time',
      },
      {
        title: '组织:',
        type: 'checkbox',
        options: [],
        key: 'orgs',
      },
      {
        title: '公告内容:',
        type: 'tinymce',
        key: 'memo',
        placeholder: '请输入内容',
        maxlength: 200,
      },
      {
        title: '附件:',
        type: 'file',
        limit: 1,//上传限制
        multiple: true,//多选
        key: 'attachments',
        fileUrl: 'http://travel.file.zhhost.top/line/',
      },
    ],
    noticeForms: {// 编辑/新增 表单提交
      id: 0,
      title: '',
      subtitle: '',
      notice_time: '',
      orgs: [],
      memo: '',
      attachments_old: '',//原附件
      attachments: '',
      readname:'',
    },
    noticeRules: {// 编辑/新增 表单验证规则
      title: [{ required: true, message: '请输入标题', trigger: 'change' }],
      subtitle: [{ required: true, message: '请输入副标题', trigger: 'change' }],
      memo: [{ required: true, message: '请输入内容', trigger: 'blur' }],
      // orgs: [{ required: true, message: '请选择组织', trigger: 'blur' }]
    },
    renderForms: [// 编辑/新增 表单渲染
      {
        title: '输入框:',
        type: 'input',
        inputType: 'number',
        key: 'inputV',
        placeholder: '请输入内容',
        maxlength: 2,
      },
      {
        title: '自定义:',
        type: 'custom',
        key: 'custom1',
      },
      {
        title: '多行输入框:',
        type: 'input',
        inputType: 'textarea',
        key: 'textareaV',
        placeholder: '请输入内容',
        maxlength: 2,
      },
      {
        title: '数字计数器:',
        type: 'inputNumber',
        key: 'inputNumberV',
        placeholder: '请输入内容',
        maxlength: 2,
      },
      {
        title: 'rate评分:',
        type: 'rate',
        key: 'rateV',
        allowHalf: true,
      },
      {
        title: '下拉选择:',
        type: 'select',
        options: [
          { value: 0, label: '选项a' },
          { value: 1, label: '选项b' },
          { value: 2, label: '选项c' },
        ],
        key: 'selectV',
        placeholder: '请选择',
      },
      {
        title: '开关:',
        type: 'switch',
        key: 'switchV',
        activetext: '按月付费',
        intext: '按年付费',
      },
      {
        title: '多选:',
        type: 'checkbox',
        options: ['上海', '北京', '广州', '深圳'],
        key: 'checkboxV',
      },
      {
        title: '时间:',
        type: 'time',
        key: 'timeV',
      },
      {
        title: '日期:',
        type: 'date',
        key: 'dateV',
      },
      {
        title: '日期范围:',
        type: 'dateTime',
        key: 'dateTimeV',
      },
      {
        title: '上传:',
        type: 'upload',
        limit: 2,//上传限制
        multiple: true,//多选
        key: 'uploadV',
        fileUrl: 'http://travel.file.zhhost.top/line/',
      },
      {
        title: '上传:',
        type: 'upload',
        limit: 3,//上传限制
        multiple: true,//多选
        key: 'uploadV1',
        fileUrl: 'http://travel.file.zhhost.top/line/',
      },
    ],
    // 文档管理
    documentRenderForms: [
      {
        title: '办文标题:',
        type: 'input',
        inputType: 'text',
        key: 'title',
        placeholder: '请输入办文标题',
        maxlength: 20,
      },
      {
        title: '下发时间:',
        type: 'date',
        key: 'doc_time',
      },
      {
        title: '发件人:',
        type: 'input',
        inputType: 'text',
        disabled: true,
        key: 'source',
        placeholder: '请输入发件人',
      },
      {
        title: '科室核审:',
        type: 'cascader',
        key: 'org_id',
        checkStrictly: true,//可单选
        showalllevels: false,//仅显示选中
        placeholder: '请选择',
        show: true,
        clearable: false
      },
      {
        title: '科室审核人:',
        type: 'select',
        options: [],
        key: 'approve_user1',
        placeholder: '请选择',
        show: false,
      },
      {
        title: '分管领导审批:',
        type: 'select',
        options: [],
        key: 'approve_user2',
        placeholder: '请选择',
        show: false,
      },
      {
        title: '街道主要领导审批:',
        type: 'select',
        options: [],
        key: 'approve_user3',
        placeholder: '请选择',
        show: false,
      },
      {
        title: '承办人组织:',
        type: 'cascader',
        key: 'approve_user4_org',
        checkStrictly: true,//可单选
        showalllevels: false,//仅显示选中
        placeholder: '请选择组织',
        show: false,
        clearable: false
      },
      {
        title: '承办人:',
        type: 'select',
        options: [],
        key: 'approve_user4',
        placeholder: '请选择',
        show: false,
      },
      {
        title: '拟办意见:',
        type: 'input',
        inputType: 'textarea',
        key: 'memo',
        placeholder: '请输入拟办意见',
      },
      {
        title: '附件:',
        type: 'file',
        limit: 1,//上传限制
        multiple: false,//多选
        key: 'attachments',
        fileUrl: 'http://travel.file.zhhost.top/line/',
      },
    ],
    documentForms: {// 编辑/新增 表单提交
      id: '0',
      title: '',
      source: '',
      doc_time: '',
      memo: '',
      attachments_old: '',
      attachments: '',
      readname: '',//下发的办文附件
      org_id: 0,
      approve_user1:"",
      approve_user2:"",
      approve_user3:"",
      approve_user4_org:"",
      approve_user4:""
    },
    documentRules: {// 编辑/新增 表单验证规则
      title: [{ required: true, message: '请输入办文标题', trigger: 'change' }],
      doc_time: [{ required: true, message: '请选择下发时间', trigger: 'change' }],
      source: [{ required: true, message: '请输入下发科室/单位或登录账号名称', trigger: 'change' }],
      org_id: [{ required: true, message: '请选择', trigger: 'change' }],
      memo: [{ required: true, message: '请输入拟办意见', trigger: 'change' }],
      approve_user4_org: [{ required: true, message: '请选择', trigger: 'change' }],
      approve_user4: [{ required: true, message: '请选择', trigger: 'change' }],
    },
  }
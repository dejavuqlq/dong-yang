
import dayjs from "dayjs";
// 域名
export const host = () => {
  return "http://partybuilding.server.zhhost.top/zh8848/"
}
// 配置文件域名
export const fileHost = () => {
  return "https://partybuilding.server.zhhost.top/upload/"
};
// 配置文件域名-oss
export const ossHost = () => {
  return 'http://oss-cn-hangzhou.aliyuncs.com/notice/'
};
// 本地未上传地址
export const beforeUpload = () => {
  return "http://partybuilding.server.zhhost.top/"
};
// 上传文件域名
export const uploadHost = () => {
  return "http://partybuilding.server.zhhost.top/zh8848/upload"
};
// 图片临时回显地址
export const imagesDisplayHost = () => {
    return "http://partybuilding.server.zhhost.top/upload/"
};
// 文档管理下载
export const filedownload = () => {
  return "http://party--building.oss-cn-hangzhou.aliyuncs.com/files/"
}
// 通知公告下载
export const noticedownload = () => {
  return "http://party--building.oss-cn-hangzhou.aliyuncs.com/notice/"
}
// 办文审批文件下载
export const documentdownload = () => {
  return "http://party--building.oss-cn-hangzhou.aliyuncs.com/document/"
}
// 轮播图
export const configdownload = () => {
  return "http://party--building.oss-cn-hangzhou.aliyuncs.com/banner/"
}
// 意见反馈
export const proposalImg = () => {
  return "http://party--building.oss-cn-hangzhou.aliyuncs.com/config/"
}

// 校验身份证
export const checkIdCard = (idCard = '') => {
  // areaCode:地区码  checkCode：最后一位的校验码
  const areaCode = [11, 12, 13, 14, 15, 21, 22, 23, 31, 32, 33, 34, 35, 36, 37, 41, 42, 43, 44, 45, 46, 50, 51, 52, 53, 54, 61, 62, 63, 64, 65, 71, 81, 82, 91];
  const checkCode = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
  if (idCard.length === 15) {// 如果是15位的身份证号码
    // 判断地区码
    const idCardAreaCode = parseInt(idCard.substr(0, 2));
    if (areaCode.indexOf(idCardAreaCode) === -1) {
      return false;
    }
    // 判断时间
    const borthYear = parseInt(idCard.substr(6, 2)) + 1900;
    const isRunNian = (borthYear % 400 === 0) || (borthYear % 100 !== 0 && borthYear % 4 === 0);
    let regStr = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$/;
    if (isRunNian) {
      regStr = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$/;
    }
    if (!idCard.match(regStr)) {
      return false;
    }
    return true;
  } else if (idCard.length === 18) {        // 如果是18位的身份证号码
    // 判断地区码
    const idCardAreaCode = parseInt(idCard.substr(0, 2));
    if (areaCode.indexOf(idCardAreaCode) === -1) {
      return false;
    }
    // 判断时间
    const borthYear = parseInt(idCard.substr(6, 4));
    const isRunNian = (borthYear % 400 === 0) || (borthYear % 100 !== 0 && borthYear % 4 === 0);
    let regStr = /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$/;
    if (isRunNian) {
      regStr = /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$/;
    }
    if (!idCard.match(regStr)) {
      return false;
    }
    // 判断最后一位---校验码
    const sumIdCard = (parseInt(idCard.substr(0, 1)) + parseInt(idCard.substr(10, 1))) * 7 +
      (parseInt(idCard.substr(1, 1)) + parseInt(idCard.substr(11, 1))) * 9 +
      (parseInt(idCard.substr(2, 1)) + parseInt(idCard.substr(12, 1))) * 10 +
      (parseInt(idCard.substr(3, 1)) + parseInt(idCard.substr(13, 1))) * 5 +
      (parseInt(idCard.substr(4, 1)) + parseInt(idCard.substr(14, 1))) * 8 +
      (parseInt(idCard.substr(5, 1)) + parseInt(idCard.substr(15, 1))) * 4 +
      (parseInt(idCard.substr(6, 1)) + parseInt(idCard.substr(16, 1))) * 2 +
      parseInt(idCard.substr(7, 1)) * 1 + parseInt(idCard.substr(8, 1)) * 6 +
      parseInt(idCard.substr(9, 1)) * 3;
    const modNum = checkCode[sumIdCard % 11];
    if (modNum !== idCard.substr(17, 1).toUpperCase()) {
      return false;
    }
    return true
  } else {
    return false;
  }
};

// 校验手机号
export const phone = (num = '') => {
  let telReg = /^[1][3,4,5,7,8][0-9]{9}$/;
  return telReg.test(num)
}

// 快捷时间选项
export const shortcuts = () => {
  return [{
    text: '最近一周',
    value: (() => {
      const end = new Date();
      const start = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
      return [start, end]
    })()
  }, {
    text: '最近一个月',
    value: (() => {
      const end = new Date();
      const start = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
      return [start, end]
    })()
  }, {
    text: '最近三个月',
    value: (() => {
      const end = new Date();
      const start = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
      return [start, end]
    })()
  }]
};

// 设置cookie
export const setCookie = (name: string, value: string, day: number = 30) => {
  let Days = 30;
  let exp = new Date();
  exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
  document.cookie = name + "=" + escape(value) + ";expires=" + exp.toUTCString();
};

// 获取cookie
export const getCookie = (name: string) => {
  let prefix = name + "="
  let start = document.cookie.indexOf(prefix)
  if (start == -1) {
    return null;
  }
  let end = document.cookie.indexOf(";", start + prefix.length)
  if (end == -1) {
    end = document.cookie.length;
  }
  let value = document.cookie.substring(start + prefix.length, end)
  return unescape(value);
};

// 删除cookie
export const delCookie = (name: string) => {
  let arr;
  let reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
  // eslint-disable-next-line no-cond-assign
  if (arr = document.cookie.match(reg))
    return unescape(arr[2]);
  else
    return null;
};

// 获取地址栏参数
export const setectQuery = (name: string = '') => {
  let href: string = window.location.href;
  let strArr: Array<string> = href.split("?");
  if (strArr.length === 0) {
    return "没有参数"
  } else if (strArr.length > 2) {
    return "Error:发现多个 ？"
  } else {
    let queryArr: Array<string> = strArr[1].split("&"),
        obj: any = {};
    queryArr.map((item: string, index: number) => {
      let o: Array<string> = item.split("=");
      obj[o[0]] = o[1]
    })
    if (name) {
      return obj[name]
    } else {
      return obj
    }
  }
};

// 日期格式化
export const formatDate = (date: string = '') => {
  if (date) {
    let newDate = new Date(date);

    let year = newDate.getFullYear().toString();
    let month = (newDate.getMonth() + 1).toString();
    let day = newDate.getDate().toString();
    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }
    return `${year}-${month}-${day}`;
  } else {
    return ''
  }
};

// 获取当前日期
export const getDate = (minute: Boolean) => {
  let yy = new Date().getFullYear()
  let mm = (new Date().getMonth() + 1) < 10 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1)
  let dd = new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate()
  let hh = new Date().getHours() < 10 ? '0' + new Date().getHours() : new Date().getHours()
  let mf = new Date().getMinutes() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes()
  let ss = new Date().getSeconds() < 10 ? '0' + new Date().getSeconds() : new Date().getSeconds()
  let date = ''
  if (minute) {
  // 需要时分秒
    date = yy + '-' + mm + '-' + dd + ' ' + hh + ':' + mf + ':' + ss
  } else {
    // 不需要时分秒
    date = yy + '-' + mm + '-' + dd 
  }
  return date
}

// 与运算 数组转int arr的数组必须是2的倍数
export const arrToInt = (arr: Array<number>) => {
  if (arr.length === 0) {
    return {val: '-1', msg: '数组为空'}
  } else {
    // 判断数组是否为2的倍数
    let flag = true;
    arr.map((item) => {
        let f = check(item);
        if(!f){
          flag = false;
        }
    });
    if (flag) {
      let o = arr[0];
      for (let i = 1; i < arr.length; i++) {
        o = o | arr[i]
      }
      return {val: '0', msg: o}
    } else {
      return {val: '-1', msg: '数组某项值不是2的n次幂，请检查数组'}
    }
  }

};

export const check=(num:number)=>{
  if(num != 1){
    while(num != 1){
      if(num%2 == 0){
        num = num / 2;
      }else{
        return false;
      }
    }
    return true;
  }else{
    return true;
  }
}

// 反向 & 0代表false 非0代表true //*处理逻辑 传入原数组 逐个对比 非0代表选中 [arr的规则]
export const intToArr = (num:number,arr:Array<number>)=>{
  let newArr:Array<number>=[];
  if(arr.length>0){
    // 判断数组是否为2的倍数
    let flag = true;
    arr.map((item) => {
      let f = check(item);
      if(!f){
        flag = false;
      }
    });
    if (flag) {
      arr.map((item) => {
        if((num & item)!==0){
          newArr.push(item);
        }
      });
      return {val: '0', msg: newArr }
    } else {
      return {val: '-1', msg: '数组某项值不是2的n次幂，请检查数组'}
    }
  }else{
    return {val:"-1",msg:'数组为空'};
  }
};

// 时间转字符串 parseDate2Str
export const parseDate2Str = (date:any,format:any="YYYY-MM-DD HH:mm:ss")=>{
  format = format || "YYYY-MM-DD HH:mm:ss";
  return dayjs(date).format(format);
};




import { createApp } from 'vue';
import App from './App.vue';
import ElementPlus from 'element-plus';
// import '../theme/index.css'//深度自定义主题 抽屉弹出有问题没解决/其他位置问题 慎用
import './element-variables.scss';//折中 主题公共样式

import store from './store';
// @ts-ignore
import axios from './plugins/axios.ts';
import router from './router';
const app = createApp(App);
app.use(ElementPlus, {}).use(store).use(router).use(axios).mount('#app');
